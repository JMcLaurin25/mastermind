#ifndef COMPPROCS_H
#define COMPPROCS_H

void find_nums(int *input, int *secret, const int *sz_code, int *sums, int *num_found, int *count);
void swap(int *, int *, int *);

#endif
