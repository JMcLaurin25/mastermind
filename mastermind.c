#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "guessprocs.h"
#include "compprocs.h"

#define SZ_IN 6

int main(int argc, char **argv)
{
	//sums[0] = red, sums[1] = white
	int input[SZ_IN], secret[SZ_IN], sums[2] = {0}; 
	int count = 0;
	const int sz_code = 4;

	create_sequence(secret, &sz_code);
	printf("> mastermind\n");

	switch (argc) {
		case 1:
			while (sums[0] != sz_code) {
				if (!user_guess(input, &sz_code)) { //Valid inputs
					compare(secret, input, &sz_code, sums);
					count++;
				}
			}
			break;

		case 2:
			if (strcmp(argv[1], "-a")) {
				printf("./mastermind [-a automate]\n");
			} else {
				int num_found[SZ_IN] = {0};
				int index_inner = 0;

				find_nums(input, secret, &sz_code, sums, num_found, &count);

				found: while (sums[0] != sz_code) {

					for (int outer = 0; outer < sz_code; outer++) {
						for (int inner = 0; inner < sz_code; inner++) {
							if (num_found[outer] == num_found[inner]) {
								break;
							}
							swap(num_found, &outer, &inner);//Swap found numbers.

							//Print current line.
							printf(">> Computer guess: ");
							sleep(1);
							for (index_inner = 0; index_inner < sz_code; index_inner++) {
								printf("%d", num_found[index_inner]);
							}
							printf("\n");

							compare(secret, num_found, &sz_code, sums);
							
							if (sums[0] == 4) { // Check for all reds.
								goto found;
							}
							count++;
						}
					}
				}
			}
			break;
		default:
			break;
	}


	if (sums[0] == 4) {
		printf("You win!  It took you %d guesses, to find ", count);
		for (int index = 0; index < sz_code; index++) {
			printf("%d", secret[index]);
		}
		printf("\n");
	}
}
















