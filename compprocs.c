#include <stdio.h>
#include <unistd.h>
#include "guessprocs.h"

void find_nums(int *input, int *secret, const int *sz_code, int *sums, int *num_found, int *count)
{
	int numbers[] = {0,1,2,3,4,5,6,7,8,9};
	int index_outer = 0, index_inner = 0, count_found = 0;
	*count = 0;
		
	while (count_found != 4) { //Find the numbers, store in num_found

		for (index_inner = 0; index_inner < *sz_code; index_inner++) {
			input[index_inner] = numbers[index_outer];
		}

		//Print current guess
		printf(">> Computer guess: ");
		sleep(1);
		for (index_inner = 0; index_inner < *sz_code; index_inner++) {
			printf("%d", input[index_inner]);
		}
		printf("\n");

		compare(secret, input, sz_code, sums);

		if(sums[0] > 0) { //store found number
			for (index_inner = 0; index_inner < sums[0]; index_inner++) {
				num_found[count_found] = numbers[index_outer];
				count_found++;
			}
		}
		*count+= 1;
		index_outer++;
	}
}

void swap(int *num_found, int *outer, int *inner)
{
	int temp; //Temp storage for swap
	temp = num_found[*outer];
	num_found[*outer] = num_found[*inner];
	num_found[*inner] = temp;
}
