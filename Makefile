CFLAGS+=-std=c11 -Werror -Wno-deprecated -pedantic -Wall -Wextra -pedantic -Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline

mastermind: mastermind.o guessprocs.o compprocs.o

compprocs.o: compprocs.c

guessprocs.o: guessprocs.c

mastermind.o: mastermind.c

.PHONY: clean debug

clean:
	rm *.o

debug: CFLAGS+=-g
debug: mastermind
