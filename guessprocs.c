#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>


/* LOCAL FUNCTION PROTOTYPES */
void find_red(int *, int *guess, int *key, const int *);
void find_white(int *, int *, int *, int *, const int *);

#define SZ_GUESS 6
#define LENGTH 4

void create_sequence(int *answer, const int *sz_code)// Generate the 4 random numbers
{
	int i;
	srand(time(NULL));
	for(i = 0; i < *sz_code; i++) {
		answer[i] = (rand() % 10);
	}
}

int user_guess(int *guess, const int *sz_code) //Convert chars to int, check for validity
{
	int index;
	char input[SZ_GUESS];

	printf("Guess a number: ");
	scanf("%5s", input);

	while(getchar() != '\n');

	if (input[4] != '\0' || strlen(input) != 4) { //Correct size verification
		printf("Guess must be 4 numbers.\n");
		return 1;
	}


	for(index = 0; index < *sz_code; index++) { //Numerical verification
		if ((input[index] < '0') || (input[index] > '9')) {
			printf("Invalid Entry!\n");
			return 1;
		} else {
			guess[index] = input[index] - '0';
		}
	}
	return 0;
}

void compare(int *answer, int *guess, const int *sz_code, int *sums)
{
	int red[LENGTH] = {0}, white[LENGTH] = {0}; //Red-White masks with 4 zeros
	int indx_cmp;

	//reset sums
	sums[0] = 0;
	sums[1] = 0;

	find_red(red, guess, answer, sz_code);
	find_white(white, red, guess, answer, sz_code);

	for (indx_cmp = 0; indx_cmp < *sz_code; indx_cmp++) {
		if (red[indx_cmp] == 1) {
			sums[0]++;
		} else if (white[indx_cmp] == 1) {
			sums[1]++;
		}
	}

	printf("%d red, %d white\n", sums[0], sums[1]);
}

void find_red(int *red, int *guess, int *key, const int *sz_code) //Create red mask
{
	int indx_red;

	for (indx_red = 0; indx_red < *sz_code; indx_red++) {
		if (guess[indx_red] == key[indx_red]) {
			red[indx_red] = 1;
		}
	}
}

void find_white(int white[], int red[], int guess[], int key[], const int *sz_code)
{
	int indx_key, indx_guess;

	for (indx_guess = 0; indx_guess < *sz_code; indx_guess++) {
		if (!red[indx_guess]) { //Not masked by red at guess index

			for (indx_key = 0; indx_key < *sz_code; indx_key++) {
				if ((!red[indx_key]) && (!white[indx_key])) { //not masked by key for red and white

					if ((guess[indx_guess] == key[indx_key])) {
						white[indx_key] = 1; //Change white mask
						break;
					}
				} 
			}
		}
	}
}
