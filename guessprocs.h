#ifndef GUESSPROCS_H
#define GUESSPROCS_H

void create_sequence(int *, const int *);
int user_guess(int *, const int *);
void compare(int *answer, int *guess, const int*, int []);

#endif
